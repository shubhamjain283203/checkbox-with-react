// import { render, screen } from '@testing-library/react';
// import App from './App';

// it('renders learn react link', () => {
//   const { container, getByText }  = render(<App/>);
//   expect(getByText('shubham')).toBeInTheDocument()

//   // debug()
//   // const linkElement = screen.getByText(/learn react/i);
//   // expect(linkElement).toBeInTheDocument();
// });

import { render, screen } from '@testing-library/react';
import configureStore from "redux-mock-store";
import { Provider } from 'react-redux';
import App from './App';

test('renders learn react link', () => {
  const mockStore = configureStore();
  const store = mockStore({});
  render(<Provider store={store}><App /></Provider>);
  // expect(getByText('shubham')).toBeInTheDocument()
});
