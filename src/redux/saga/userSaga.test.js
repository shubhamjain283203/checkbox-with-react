import { put, takeEvery } from 'redux-saga/effects';
import {FromApi, userSaga} from './userSaga'

describe('SAGAS', () => {
  const generator = userSaga();
  it('should dispatch action "GET_USERS_REQUESTED" ', () => {
    expect(generator.next().value).toEqual(takeEvery('GET_USERS_REQUESTED', FromApi))
  })
  it('should be done on next iteration', () => {
    expect(generator.next().done).toBeTruthy();
  });
})
