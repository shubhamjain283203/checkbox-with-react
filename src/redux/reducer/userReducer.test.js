import userReducer from "./userReducer";
import * as type from '../types';

const initialState = {
    search: [],
    loading: false,
    error: null,
  }
  
describe('REDUCER', () => {

  it('should return the initial state', () => {
    expect(userReducer(initialState, {})).toEqual(initialState)
  })

  it('should handle "GET_NEWS" action', () => {
    expect(userReducer({initialState, loading: true}, { type: type.GET_USERS_REQUESTED })).toEqual({ initialState,loading: true })
  })

  it('should handle "NEWS_RECEIVED" action', () => {
    const search = [{
      "author": "Shubham Jain",
      "title": "Reducer Testing",
      "description": "Test get successfull"
    }];
    const newSate = userReducer(initialState, { type: type.GET_USERS_SUCCESS, payload: search })
    expect(newSate).toEqual({...initialState, search})
  })
})